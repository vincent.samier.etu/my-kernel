#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "miniio.h"
#include "scheduler.h"
#include "stmanip.h"

#define NULL ((void *)0)
#define STACK_SIZE  8192
#define MAX_WAITING_PROCESS		10
#define	BUFFER_SIZE 255

struct ctx_s *current;
struct ctx_s *head;
struct ctx_s *tail;

struct ctx_s context1;
struct ctx_s context2;
char pile_ctx_1[STACK_SIZE];
char pile_ctx_2[STACK_SIZE];

unsigned int cpt = 0;

struct sem_s sem;
struct sem_s mutex_acces_buffer;
char buffer_input[BUFFER_SIZE];
int nbChar = 0;
int posLastRead = 0;

void empty_irq(int_regs_t *r) {
}

/**
 * Libère une ressource du sémaphore et notifie les processus éventuellement en attente
 * */
void sem_up(struct sem_s *s) {
    __asm volatile("cli");
	s->available++;
	if (s->first != NULL) {
		s->first->state = CTX_STARTED;
		s->first = s->first->nextSleeping;
	}		
    __asm volatile("sti");
}

void sem_down(struct sem_s *s) {
    __asm volatile("cli");
	if (s->available == 0) {
		current->state = CTX_WAITING;
		if (s->first == NULL) {
			s->first = current;
			s->last = current;
		} else {
			struct ctx_s *tmp = s->last;
			s->last = current;
			tmp->nextSleeping = current;
		}
		yield();
	}
	s->available--;
    __asm volatile("sti");
}

/**
 * Initialise un sémaphore avec un compteur maximum de val.
 * */
void sem_init(struct sem_s *sem, int val) {
	sem->available = val;
	sem->first = sem->last = NULL;
}

/**
 * Map un scan code avec un code ascii si possible
 * Un code peut correspondre à la pression ou relachement d'une touche
 * Mapping pour un clavier QWERTY
 * */
char keyboard_map(unsigned char c) {
	switch (c)
	{
		case 0x02:
    		return '1';
    		break;
		case 0x03:
    		return '2';
    		break;
		case 0x04:
			return '3';
			break;
		case 0x05:
    		return '4';
    		break;
		case 0x06:
    		return '5';
    		break;
		case 0x07:
    		return '6';
    		break;
		case 0x08:
			return '7';
			break;
		case 0x09:
    		return '8';
    		break;
		case 0x0A:
    		return '9';
    		break;
		case 0x0B:
    		return '0';
    		break;
		case 0x0C:
    		return '-';
    		break;
		case 0x0D:
    		return '=';
    		break;
		case 0x10:
			return 'Q';
			break;
		case 0x11:
			return 'W';
			break;  
		case 0x12:
			return 'E';
			break;
		case 0x13:
			return 'R';
			break; 
		case 0x14:
    		return 'T';
    		break;
		case 0x15:
			return 'Y';
			break; 
		case 0x16:
			return 'U';
			break;
		case 0x17:
			return 'I';
			break;  
		case 0x18:
    		return 'O';
    		break;
		case 0x19:
			return 'P';
			break;
		case 0x1A:
			return '[';
			break; 
		case 0x1B:
			return ']';
			break;
		case 0x1E:
			return 'A';
			break;
		case 0x1F:
			return 'S';
			break; 
		case 0x20:
    		return 'D';
    		break;
		case 0x21:
    		return 'F';
    		break;
		case 0x22:
    		return 'G';
    		break;
		case 0x23:
    		return 'H';
    		break;
		case 0x24:
    		return 'J';
    		break;
		case 0x25:
    		return 'K';
    		break;
		case 0x26:
    		return 'L';
    		break;
		case 0x27:
    		return ';';
    		break;
		case 0x28:
    		return '\'';
    		break;
		case 0x29:
    		return '`';
    		break;
		case 0x2B:
    		return '\\';
    		break;
		case 0x2C:
    		return 'Z';
    		break;
		case 0x2D:
    		return 'X';
    		break;
		case 0x2E:
    		return 'C';
    		break;
		case 0x2F:
    		return 'V';
    		break;
		case 0x30:
    		return 'B';
    		break;
		case 0x31:
    		return 'N';
    		break;
		case 0x32:
    		return 'M';
    		break;
		case 0x33:
    		return ',';
    		break;
		case 0x34:
    		return '.';
    		break;
		case 0x35:
    		return '/';
    		break;
		case 0x37:
    		return '*';
    		break;
		case 0x39:
    		return ' ';
    		break;
		case 0x47:
    		return '7';
    		break;
		case 0x48:
    		return '8';
    		break;
		case 0x49:
    		return '9';
    		break;
		case 0x4A:
    		return '-';
    		break;
		case 0x4B:
    		return '4';
    		break;
		case 0x4C:
    		return '5';
    		break;
		case 0x4D:
    		return '6';
    		break;
		case 0x4E:
    		return '+';
    		break;
		case 0x4F:
    		return '1';
    		break;
		case 0x50:
    		return '2';
    		break;
		case 0x51:
    		return '3';
    		break;
		case 0x52:
    		return '0';
    		break;
		case 0x53:
    		return '.';
    		break;
		default:
			return 0;
			break;
	}
}

void getc() {
	sem_down(&sem);
	sem_down(&mutex_acces_buffer);
	nbChar--;
	char c = buffer_input[(posLastRead++)%BUFFER_SIZE];
	if (c != 0) {
		putc(c);
	}
	sem_up(&mutex_acces_buffer);
}

void input_pressed_handler() {
	sem_down(&mutex_acces_buffer);
	char c = keyboard_map(_inb(0x60));
	buffer_input[(posLastRead + nbChar++) % BUFFER_SIZE] = c;
	sem_up(&sem);
	sem_up(&mutex_acces_buffer);
}

int create_ctx(struct ctx_s *ctx, char *stack, func_t f, void *args) {
    ctx->pile = stack;
    ctx->sp = ctx->pile+STACK_SIZE - sizeof(void *);
    ctx->bp = ctx->pile+STACK_SIZE - sizeof(void *);
    ctx->magic = MAGIC_CTX;
    ctx->pf = f;
    ctx->args = args;
    ctx->state = CTX_READY;
	ctx->nextSleeping = NULL;
    if (head != NULL) {
        head->next = ctx;
    } else {
        tail = ctx;
    }
    head = ctx;
    head->next = tail;
    return 1;
}

void yield() {
    if (current != NULL) {
        READ_CTX(current->bp, current->sp);
		do {
			current = current->next;
		} while (current->state == CTX_WAITING);
    } else {
        current = tail;   
    }
    WRITE_CTX(current->bp, current->sp);
    if (current->state == CTX_READY) {
        current->state = CTX_STARTED;
        current->pf(current->args);
    }
    return;
}

void print_clavier(void *args)
{
    while(1) {
        getc();
    }
}

void f_cpt(void *args)
{
    while(1) {
		__asm volatile("cli");
		int cursor_x_cp=cursor_x;					/* here is the cursor position on X [0..79] */
		int cursor_y_cp=cursor_y;	
		cursor_x = 70;	
		cursor_y = 5;
        puthex(cpt++);
		cursor_x = cursor_x_cp;
		cursor_y = cursor_y_cp;
		_outb(0xA0, 0x20);
		_outb(0x20, 0x20);
		__asm volatile("sti");
    }
}

static void timer_it() {
	__asm volatile("cli");
	_outb(0xA0, 0x20);
	_outb(0x20, 0x20);
	__asm volatile("sti");
	yield();
}


/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    clear_screen();
    puts("Early boot.\n"); 
    puts("\t-> Setting up the GDT... ");
    gdt_init_default();
    puts("OK\n");

    puts("\t-> Setting up the IDT... ");
    setup_idt();
    puts("OK\n");

    puts("\n\n");
    idt_setup_handler(0, timer_it); 
    
	idt_setup_handler(1, input_pressed_handler); 

    create_ctx(&context1, pile_ctx_1, print_clavier, NULL);
    create_ctx(&context2, pile_ctx_2, f_cpt, NULL);

	sem_init(&sem, 0);
	sem_init(&mutex_acces_buffer, 1);
 	 
	__asm volatile("sti");

    /* minimal setup done ! */

    for(;;) {
	}
}
