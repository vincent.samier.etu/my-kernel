#define MAGIC_CTX 0xDEADBEEF

typedef void (*func_t)(void *);

void yield();

struct ctx_s {
    uint64_t magic;
    void *sp;
    void *bp;
    void *pile;
    func_t pf;
    void *args;
    int state;
    struct ctx_s *next;
    struct ctx_s *nextSleeping;
};

enum state_ctx {
    CTX_READY,
    CTX_STARTED,
    CTX_WAITING,
    CTX_TERMINATED
};

struct sem_s
{
    int available;
    struct ctx_s *first;
    struct ctx_s *last;
};
