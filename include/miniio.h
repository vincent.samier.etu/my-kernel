#ifndef _MINIIO_H_
#define _MINIIO_H_

void clear_screen();				/* clear screen */
void putc(char aChar);				/* print a single char on screen */
void puts(char *aString);			/* print a string on the screen */
void puthex(unsigned aNumber);			/* print an Hex number on screen */
void putdec(unsigned aNumber);			/* print a decimal number on screen */

extern int cursor_x;
extern int cursor_y;	

#endif
